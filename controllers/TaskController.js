const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {
	// My solution
	/*return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		let taskName = result.name

		return Task.deleteOne({_id: task_id}).then((deletedTask, error) => {
			return `Task '${taskName}' was successfully deleted!`
		})
	})*/

	// Better solution
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if(error){
			console.log(error)
			return error
		}

		return removedTask
	})
}

// Activity

module.exports.getTask = (task_id) => {
	return Task.findById(task_id).then((result) => {
		return result
	})
}

module.exports.completeTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.status = 'complete'

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}