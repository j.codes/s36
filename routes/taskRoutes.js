const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch("/:id/update", (request, response) => { // :id = URL parameter
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete task
router.delete("/:id/delete", (request, response) => { 
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// Activity routes

// Getting specific task
router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// Updating the status of a task
router.put("/:id/complete", (request, response) => { // :id = URL parameter
	TaskController.completeTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

module.exports = router // exports the router/file so it can be used by other files